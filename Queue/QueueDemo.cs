﻿namespace Queue
{
    public class DemoQueue
    {
        public int[] Items;
        public int MaxQueue;
        public int IndexOfLastItemInQueue;

        // Constructor khởi tạo queue với số lượng item và gán IndexOfLastItemInQueue = -1
        public DemoQueue(int maxQueue)
        {
            MaxQueue = maxQueue;
            Items = new int[MaxQueue];
            IndexOfLastItemInQueue = -1; // Đánh dấu Queue là trống.
        }

        // Thêm một phần tử vào Queue
        public void EnQueue(int item)
        {
            // Kiểm tra nếu Queue đã đầy
            if (IsFull())
            {
                throw new Exception("Queue is full");
            }
            // Tăng giá trị IndexOfLastItemInQueue lên 1
            IndexOfLastItemInQueue++;
            // Gán phần tử item vào vị trí IndexOfLastItemInQueue trong mảng Items
            Items[IndexOfLastItemInQueue] = item;
        }

        // Loại bỏ phần tử đầu Queue và trả về giá trị của phần tử đó
        public int DeQueue()
        {
            // Kiểm tra nếu Queue rỗng
            if (IsEmpty())
            {
                throw new Exception("Queue is empty");
            }

            // Gán item bằng giá trị của phần tử tại vị trí đầu tiên của mảng
            int item = Items[0];
            // Di chuyển toàn bộ các phần tử lên phía trước 1 đơn vị
            for (int i = 0; i < IndexOfLastItemInQueue; i++)
            {
                Items[i] = Items[i + 1];
            }
            // Giảm giá trị IndexOfLastItemInQueue đi 1 đơn vị
            IndexOfLastItemInQueue--;
            // Trả về giá trị của phần tử đầu Queue
            return item;
        }

        // Trả về giá trị của phần tử đầu Queue mà không xóa nó
        public int Front()
        {
            // Kiểm tra nếu Queue rỗng
            if (IsEmpty())
            {
                throw new Exception("Queue is empty");
            }
            // Trả về giá trị của phần tử tại vị trí đầu tiên của mảng
            return Items[0];
        }

        // Kiểm tra xem Queue có rỗng không
        public bool IsEmpty()
        {
            return IndexOfLastItemInQueue == -1;
        }

        // Kiểm tra xem Queue có đầy không
        public bool IsFull()
        {
            return IndexOfLastItemInQueue == MaxQueue - 1;
        }

        // Đếm số lượng phần tử trong Queue
        public int Count()
        {
            return IndexOfLastItemInQueue + 1;
        }


    }
}
