# Queue (Hàng đợi)

Queue là một cấu trúc dữ liệu tuyến tính tuân theo nguyên tắc First-In-First-Out (FIFO), nghĩa là phần tử đầu tiên được thêm vào hàng đợi (queue) sẽ là phần tử đầu tiên được lấy ra.

*Dưới đây là giải thích chi tiết từng bước của pseudocode để triển khai Queue:*

## Khởi tạo một class để giả lập queue
Với các thuộc tính sau:

-   `int [] Items`: Mảng để lưu trữ dữ liệu của Queue.
-   `int MaxQueue`: Kích thước tối đa của Queue.
-   `int IndexOfLastItemInQueue`: Vị trí của phần tử cuối cùng trong Queue. Nếu bằng -1, nghĩa là Queue rỗng và đang trống.

## Constructor queue với yêu cầu
Tạo ra một Queue với số lượng item và gán `IndexOfLastItemInQueue = -1`
Khởi tạo một Queue mới với các giá trị ban đầu:

-   `Items`: Khởi tạo mảng với số lượng phần tử `MaxQueue` là tham số truyền vào từ bên ngoài.
-   `IndexOfLastItemInQueue = -1`: Đánh dấu Queue là trống.

## Các thao tác cơ bản trên Queue:

### void Enqueue(int item) - Thêm một phần tử vào Queue:

-   Kiểm tra nếu Queue đã đầy (`IndexOfLastItemInQueue == MaxQueue - 1`):
    -   `throw exception "Queue is full"`.
-   Ngược lại:
    -   Tăng giá trị `IndexOfLastItemInQueue` lên 1.
    -   Gán phần tử `item` vào vị trí `IndexOfLastItemInQueue` trong mảng `Items`.

### int DeQueue() - Loại bỏ phần tử đầu Queue và trả về giá trị của phần tử đó:

-   Kiểm tra nếu Queue rỗng (`IndexOfLastItemInQueue == -1`):
    -   `throw exception "Queue is empty"`.
-   Ngược lại:
    -   Gán `item` bằng giá trị của phần tử tại vị trí đầu tiên của mảng `item = Items[0]`.
    -   Di chuyển toàn bộ các phần tử lên phía trước 1 đơn vị.
    -   Giảm giá trị `IndexOfLastItemInQueue` đi 1 đơn vị.

### int Front() - Trả về giá trị của phần tử đầu Queue mà không xóa nó:

-   Kiểm tra nếu Queue rỗng (`IndexOfLastItemInQueue == -1`):
    -   `throw exception "Queue is empty"`.
-   Ngược lại:
    -   Trả về giá trị của phần tử tại vị trí đầu tiên của mảng `Items`.

### bool IsEmpty() - Kiểm tra xem Queue có rỗng không:

-   Trả về kết quả `IndexOfLastItemInQueue == -1` (nghĩa là Queue không rỗng).

### bool IsFull() - Kiểm tra xem Queue có đầy không:

-   Trả về kết quả `(IndexOfLastItemInQueue == MaxQueue - 1)` (nghĩa là Queue đã đầy).

### bool Count() - Đếm số lượng phần tử trong Queue:

-   Trả về giá trị `IndexOfLastItemInQueue + 1` (số lượng phần tử là `IndexOfLastItemInQueue + 1`).


## Code mẫu - Queue (Hàng đợi)

Đây là một ví dụ về việc triển khai class Queue trong ngôn ngữ C#:

```csharp
public class Queue
{
    public int[] Items;
    public int MaxQueue;
    public int IndexOfLastItemInQueue;

    // Constructor khởi tạo queue với số lượng item và gán IndexOfLastItemInQueue = -1
    public DemoQueue(int maxQueue)
    {
        MaxQueue = maxQueue;
        Items = new int[MaxQueue];
        IndexOfLastItemInQueue = -1; // Đánh dấu Queue là trống.
    }

    // Thêm một phần tử vào hàng đợi
    public void Enqueue(int item)
    {
        throw new NotImplementedException();
    }

    // Lấy và xóa phần tử đầu hàng đợi
    public int DeQueue()
    {
        throw new NotImplementedException();
    }

    // Trả về giá trị của phần tử đầu hàng đợi mà không xóa nó
    public int Front()
    {
        throw new NotImplementedException();
    }

    // Kiểm tra xem hàng đợi có rỗng hay không
    public bool IsEmpty()
    {
        throw new NotImplementedException();
    }

    // Kiểm tra xem hàng đợi đã đầy hay không
    public bool IsFull()
    {
        throw new NotImplementedException();
    }

    // Đếm số lượng phần tử trong hàng đợi
    public int Count()
    {
        throw new NotImplementedException();
    }
}
```

## Trong hàm main khởi tạo queue và thao tác với queue
```csharp
try
{
    // khởi tạo queue có 5 phần tử
    DemoQueue queue = new DemoQueue(5);
    // thêm vào queue
    queue.EnQueue(1);
    queue.EnQueue(2);
    queue.EnQueue(3);
    queue.EnQueue(4);
    queue.EnQueue(5);

    // lấy item trong queue ra
    Console.WriteLine("DeQueue: " + queue.DeQueue());
    Console.WriteLine("DeQueue: " + queue.DeQueue());

    // item tiếp theo lấy ra queue
    Console.WriteLine("Front: " + queue.Front());
    Console.WriteLine("DeQueue: " + queue.DeQueue());
    Console.WriteLine("DeQueue: " + queue.DeQueue());
    Console.WriteLine("DeQueue: " + queue.DeQueue());
    // gỡ hết item ra

    // thêm 1 số nữa vào queue
    queue.EnQueue(6);
    // bốc item vừa thêm ra
    Console.WriteLine("DeQueue: " + queue.DeQueue());
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}

```

### Khởi Tạo Queue và Kiểm Tra Tính Chất Ban Đầu

Kịch Bản:

1.  Khởi tạo Queue với kích thước là 3.

Kết Quả Đạt Được:

-   `IsEmpty()`: `True`.
-   `IsFull()`: `False`.
-   `Count()`: `0`.

### Thêm Phần Tử vào Queue (Enqueue)

Kịch Bản:

1.  Khởi tạo Queue với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Queue.
3.  Thực hiện thêm phần tử 20 vào Queue.

Kết Quả Đạt Được:

-   `IsEmpty()`: `False`.
-   `IsFull()`: `False`.
-   `Count()`: `2`.

### Thêm Phần Tử vào Queue cho đến khi tràn Queue

Kịch Bản:

1.  Khởi tạo Queue với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Queue.
3.  Thực hiện thêm phần tử 20 vào Queue.
4.  Thực hiện thêm phần tử 50 vào Queue.
5.  Thực hiện thêm phần tử 100 vào Queue.

Kết Quả Đạt Được:

-   `Exception("Queue is full");` khi thực hiện Enqueue phần tử thứ 4.

### Lấy Phần Tử Khỏi Queue (DeQueue)

Kịch Bản:

1.  Khởi tạo Queue với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Queue.
3.  Thực hiện thêm phần tử 20 vào Queue.
4.  Thực hiện lấy phần tử từ Queue.
5.  Kiểm tra giá trị phần tử lấy được.

Kết Quả Đạt Được:

-   Giá trị phần tử lấy được: Phù hợp.
-   `IsEmpty()`: `False`.
-   `IsFull()`: `False`.
-   `Count()`: `1`.
-   Phần tử lấy được có giá trị là `10` vì `10` vào trước.

### Lấy nhiều hơn số Phần Tử trong Queue (DeQueue)

Kịch Bản:

1.  Khởi tạo Queue với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Queue.
3.  Thực hiện thêm phần tử 20 vào Queue.
4.  Thực hiện thêm phần tử 50 vào Queue.
5.  Thực hiện gọi hàm `DeQueue()` 4 lần.

Kết Quả Đạt Được:

-   `Exception("Queue is empty");` khi gọi hàm DeQueue lần thứ 4.

### Kiểm Tra Front (Front)

Kịch Bản:

1.  Khởi tạo Queue với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Queue.
3.  Thực hiện thêm phần tử 20 vào Queue.
4.  Thực hiện gọi hàm `Front()`.

Kết Quả Đạt Được:

-   Giá trị trả về là 10.
-   `IsEmpty()`: `False`.
-   `IsFull()`: `False`.
-   `Count()`: `2` vì Front không xóa khỏi Queue.

### Kiểm Tra Xóa Hết Phần Tử

Kịch Bản:

1.  Khởi tạo Queue với kích thước là 3.
2.  Thực hiện thêm phần tử 10 vào Queue.
3.  Thực hiện thêm phần tử 20 vào Queue.
4.  Thực hiện thêm phần tử 50 vào Queue.
5.  Thực hiện lấy phần tử từ Queue cho đến khi Queue rỗng.

Kết Quả Đạt Được:

-   `IsEmpty()`: `True`.
-   `IsFull()`: `False`.
-   `Count()`: `0`.