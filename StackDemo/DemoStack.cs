﻿namespace CallStack
{
    public class DemoStack
    {
        // Mảng để lưu trữ dữ liệu của Stack
        public int[] Items;

        // Đỉnh (Top) của Stack, ban đầu được đặt là -1 để đánh dấu Stack rỗng
        public int Top;

        // Kích thước tối đa của Stack
        public int MaxStack;

        // Constructor để khởi tạo một đối tượng Stack với kích thước tối đa đã cho
        public DemoStack(int _maxStack)
        {
            MaxStack = _maxStack;

            // Khởi tạo mảng với kích thước tối đa
            Items = new int[MaxStack];

            // Đỉnh của Stack ban đầu được đặt là -1 (Stack rỗng)
            Top = -1;
        }

        // Hàm thêm phần tử vào Stack (Push)
        public void Push(int item)
        {
            // Kiểm tra xem Stack đã đầy chưa
            if (isFull())
            {
                throw new Exception("Stack is full");
            }

            // Tăng giá trị của đỉnh lên 1 và gán giá trị vào đỉnh mới
            Top++;
            Items[Top] = item;
        }

        // Hàm lấy phần tử ra khỏi Stack (Pop)
        public int Pop()
        {
            // Kiểm tra xem Stack có rỗng không
            if (isEmpty())
            {
                throw new Exception("Stack is empty");
            }

            // Lấy giá trị của phần tử tại đỉnh và giảm đỉnh đi 1
            int valueAtTop = Items[Top];
            Top--;
            return valueAtTop;
        }

        // Hàm xem giá trị của phần tử đầu Stack mà không xóa nó (Peek)
        public int Peek()
        {
            // Kiểm tra xem Stack có rỗng không
            if (isEmpty())
            {
                throw new Exception("Stack is empty");
            }

            // Trả về giá trị của phần tử tại đỉnh
            return Items[Top];
        }

        // Hàm kiểm tra xem Stack có rỗng không
        public bool isEmpty() => Top == -1;

        // Hàm kiểm tra xem Stack đã đầy chưa
        public bool isFull() => Top == MaxStack;

        // Hàm đếm số lượng phần tử trong Stack
        public int Count() => Top + 1;
    }
}
