﻿Tree tree = new Tree();

tree.InsertNode(5);
tree.InsertNode(1);
tree.InsertNode(6);
tree.InsertNode(3);
tree.InsertNode(0);
tree.InsertNode(9);
tree.InsertNode(7);

Node root = tree.Root;
Console.WriteLine();

class Tree
{
    public Node Root { get; set; }

    public void Traverse()
    {
        TraverseNLR(Root);
    }

    // duyệt cây theo thứ tự NLR (Node - Left - Right).
    public void TraverseNLR(Node node)
    {
        if (node == null)
            return;

        // In giá trị của Node hiện tại
        Console.WriteLine(node.DataOfNode);

        // Duyệt cây con bên trái
        TraverseNLR(node.LeftNode);

        // Duyệt cây con bên phải
        TraverseNLR(node.RightNode);
    }

    public void InsertNode(int dataOfNode)
    {

        // Nếu cây rỗng, gán Node mới làm Root
        if (Root == null)
        {
            Node newNode = new Node(dataOfNode);
            Root = newNode;
            return;
        }
        // Nếu cây không rỗng, thêm Node mới vào cây
        AddNodeRecursively(Root, dataOfNode);
    }

    public void AddNodeRecursively(Node currentNode, int dataOfNewNode)
    {
        // Kiểm tra trường hợp giá trị Node mới trùng với giá trị Node hiện tại
        if (dataOfNewNode == currentNode.DataOfNode)
        {
            throw new Exception("Giá trị Node đã tồn tại trong cây");
        }

        // Xác định xem Node mới sẽ thuộc cây con bên trái hay bên phải
        if (dataOfNewNode < currentNode.DataOfNode)
        {
            if (currentNode.LeftNode == null)
            {
                Node newNode = new Node(dataOfNewNode);
                currentNode.LeftNode = newNode;
                return;
            }
            AddNodeRecursively(currentNode.LeftNode, dataOfNewNode);
        }
        else
        {
            if (currentNode.RightNode == null)
            {
                Node newNode = new Node(dataOfNewNode);
                currentNode.RightNode = newNode;
                return;
            }
            AddNodeRecursively(currentNode.RightNode, dataOfNewNode);
        }
    }

    public int CalculateHeight()
    {
        // Nếu cây rỗng, chiều cao là 0
        if (Root == null)
            return 0;

        // Sử dụng hàm đệ quy để tính chiều cao của cây
        return CalculateNodeHeight(Root);
    }

    public int CalculateNodeHeight(Node currentNode)
    {
        // Nếu currentNode là null, chiều cao là 0
        if (currentNode == null)
            return 0;

        // Sử dụng đệ quy để tính chiều cao của cây con bên trái và cây con bên phải
        int leftHeight = CalculateNodeHeight(currentNode.LeftNode);
        int rightHeight = CalculateNodeHeight(currentNode.RightNode);

        // Chiều cao của currentNode là max(leftHeight, rightHeight) + 1
        return Math.Max(leftHeight, rightHeight) + 1;
    }
}

class Node
{
    public Node(int _dataOfNode) => DataOfNode = _dataOfNode; // Khởi tạo giá trị cho Node
    public int DataOfNode { get; }
    public Node LeftNode;
    public Node RightNode;
}
