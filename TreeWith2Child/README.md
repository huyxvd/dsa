# Triển khai Binary Search Tree
Khái Niệm về Binary Search Tree (BST):

Binary Search Tree (BST) là một cấu trúc dữ liệu cây trong đó mỗi nút có tối đa hai nút con, nút con bên trái có giá trị nhỏ hơn nút cha, và nút con bên phải có giá trị lớn hơn nút cha. Điều này tạo ra một sắp xếp tự nhiên trong cây, giúp tìm kiếm, chèn và xóa nhanh chóng.

Cấu Trúc của Binary Search Tree:

##  Nút (Node):

    -   Mỗi nút trong BST chứa một giá trị dữ liệu (key).
    -   Mỗi nút có thể có tối đa hai nút con: nút con bên trái và nút con bên phải.
##  Rễ (Root):

    -   Nút ở đỉnh của cây được gọi là rễ.
    -   Rễ không có nút cha.
##  Nút Con Bên Trái và Nút Con Bên Phải:

    -   Nút con bên trái có giá trị nhỏ hơn giá trị của nút cha.
    -   Nút con bên phải có giá trị lớn hơn giá trị của nút cha.
##  Dãy con trái và Dãy con phải:

    -   Tất cả các nút con của một nút cha bên trái tạo thành một dãy con trái.
    -   Tất cả các nút con của một nút cha bên phải tạo thành một dãy con phải.

### Hàm `void InsertNode(int dataOfNewNode)`

1.  Kiểm Tra Cây Rỗng:
    -   Trước khi chèn một số mới vào cây, kiểm tra xem cây đã có Root chưa.
    -   Nếu cây chưa có Root, thêm số mới vào cây và đặt nó làm gốc (Root).
2. Gọi Hàm `AddNodeRecursively(root, int dataOfNewNode)`:
xác định nơi phù hợp để chèn số mới vào cây.

### Hàm `void AddNodeRecursively(Node currentNode, int dataOfNewNode)`

1.  Nếu `dataOfNewNode == currentNode.Data`:
    -   Báo lỗi vì không thể có hai số giống nhau trong cây.
2.  Nếu `dataOfNewNode < currentNode.Data` => node mới sẽ phải chèn vào bên trái `currentNode`:
      -   nếu `currentNode.LeftNode == null` tức là node bên trái của `currentNode` bị null thì đây chính là vị trí lý tưởng để chèn.
          -   `var newNode = new Node(dataOfNewNode);`
          -   `currentNode.LeftNode = newNode;`
          -   `Kết thúc;`
      -   ngược lại gọi lại hàm `AddNodeRecursively(currentNode.LeftNode, dataOfNewNode);` để tiếp tục tìm ra chỗ lý tưởng để thêm node
3.  Nếu `dataOfNewNode > currentNode.Data` => node mới sẽ phải chèn vào bên phải `currentNode`:
      -   nếu `currentNode.RightNode == null` tức là node bên phải của `currentNode` bị null thì đây chính là vị trí lý tưởng để chèn.
          -   `var newNode = new Node(dataOfNewNode);`
          -   `currentNode.RightNode = newNode;`
          -   `Kết thúc;`
      -   ngược lại gọi lại hàm `AddNodeRecursively(currentNode.RightNode, dataOfNewNode);` để tiếp tục tìm ra chỗ lý tưởng để thêm node

### Hàm `void TraverseNLR(Node currentNode)`

1. Nếu `currentNode == null`
    - Kết thúc vì không có gì để duyệt

2. In giá trị của Node hiện tại `currentNode.Data`.

3. `TraverseNLR(currentNode.LeftNode);` Duyệt sang Node Bên Trái:

4. `TraverseNLR(currentNode.RightNode);` Duyệt sang Node Bên Phải:


### Duyệt lại cây theo NRL, LNR, LRN, RNL, RLN

### 1. `bool NodeExists(int value)`

- Mô tả: Kiểm tra xem một Node có giá trị nhất định tồn tại trong cây hay không.

### 2. `Node FindMin()`

-   Mô tả: Tìm và trả về Node có giá trị nhỏ nhất trong cây.

### 3. `Node FindMax()`
-   Mô tả: Tìm và trả về Node có giá trị lớn nhất trong cây.

### 4. `int CountNodes()`
-   Mô tả: Đếm số lượng Node trong cây.

### 5. `void BuildTreeFromArray(int[] values)`
-   Mô tả: Xây dựng cây từ một mảng các giá trị. Duyệt qua các phần tử và gọi hàm insert Node

### 6. `int CalculateHeight()`
-   Mô tả: Tính chiều cao của cây.

1. Nếu cây rỗng (Root == null), chiều cao là 0.
    1.1. Trả về 0.

2. Nếu không, sử dụng hàm đệ quy để tính chiều cao của cây.
    2.1. Gọi hàm đệ quy `CalculateNodeHeight(Root)` với Root là Node gốc.
    2.2. Trả về kết quả từ hàm đệ quy.

Hàm `int CalculateNodeHeight(Node currentNode)`:

1. Nếu currentNode là null, chiều cao là 0.
    1.1. Trả về 0.

2. Sử dụng đệ quy để tính chiều cao của cây con bên trái và cây con bên phải.
    2.1. Gọi hàm đệ quy `CalculateNodeHeight(currentNode.LeftNode)` và lưu kết quả vào leftHeight.
    2.2. Gọi hàm đệ quy `CalculateNodeHeight(currentNode.RightNode)` và lưu kết quả vào rightHeight.

3. Chiều cao của currentNode là max(leftHeight, rightHeight) + 1.
    3.1. Trả về max(leftHeight, rightHeight) + 1.

