﻿int[] arr = new int[] { 1, 3, 7, 9, 8, 0, 11, 10 };

for (int i = 0; i < arr.Length - 1; i++)
{
    int currentMin = i;
    for (int j = i + 1; j < arr.Length; j++)
    {
        if (arr[currentMin] > arr[j])
        {
            currentMin = j;
        }

        if (currentMin != i)
        {
            // swap value between current min index & i index
            int temp = arr[i];
            arr[i] = arr[currentMin];
            arr[currentMin] = temp;
        }
    }
}


foreach (var item in arr)
{
    Console.Write($"{item} ");
}